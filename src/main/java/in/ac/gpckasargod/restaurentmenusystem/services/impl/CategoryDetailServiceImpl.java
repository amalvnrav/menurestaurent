/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.services.impl;

import in.ac.gpckasargod.restaurentmenusystem.model.ui.data.CategoryDetails;
import in.ac.gpckasargod.restaurentmenusystem.service.CategoryService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class CategoryDetailServiceImpl extends ConnectionServiceImpl implements CategoryService {  
      
    
   @Override
    public CategoryDetails readCategoryDetails(Integer id) {
               CategoryDetails  categoryDetails  = null;
                       try {
                            Connection connection = getConnection();
                            Statement statement = connection.createStatement();
                            String query = "SELECT * FROM CATEGORYMENU WHERE ID="+id;
                            ResultSet resultSet = statement.executeQuery(query);                       

                           
                         while(resultSet.next()){
                                int Id =resultSet.getInt("ID");
                                String name = resultSet.getString("CATNAME");
                                String shortName = resultSet.getString("SHORTNAME");
                                categoryDetails = new CategoryDetails(id,name,shortName);
                                     
                               
                         }
                          return categoryDetails;
                       }catch(SQLException ex) {
                            Logger.getLogger(CategoryDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
                       }
                    return categoryDetails;
    }
 
      
    @Override
          public List<CategoryDetails> getAllCategoryDetailses(){
              List<CategoryDetails> CategoryDetailses = new ArrayList<>();
              try{
                  Connection connection = getConnection();
                  Statement statement = connection.createStatement();
                  String query = "SELECT * FROM CATEGORY";
                  ResultSet resultSet = statement.executeQuery(query);
                  
                  while(resultSet.next()){
                  int id = resultSet.getInt("ID");
                  String name = resultSet.getString("NAME");
                  String shortName = resultSet.getString("SHORTNAME");
                  CategoryDetails categoryDetails = new CategoryDetails(id,name,shortName);
                  CategoryDetailses.add(categoryDetails);
              }
              }catch (SQLException ex){
                       Logger.getLogger(CategoryDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
                      
                      }
                        return CategoryDetailses;
             
                  
              } 
                       
    
    @Override
              public String deleteCategoryDetails(Integer id) {
                  try {
                       Connection connection = getConnection();
                       String query = "DELETE FROM CATEGORYMENU WHERE ID =?";
                       PreparedStatement statement = connection.prepareStatement(query);
                       statement.setInt(1,id);
                       int delete = statement.executeUpdate();
                       if(delete !=1)
                           return "Delete failed";
                       else 
                           return "Deleted successfully";
                  }catch(SQLException ex) {
                      return "Delete failed";
                  }
                  }

   @Override
    public String saveCategoryDetails(String name,String shortName) {
         try {
            Connection connection = getConnection();
            Statement  statement = connection.createStatement();
                 String query = "INSERT INTO CATEGORYMENU (CATNAME,SHORTNAME) VALUES ('"+name+"','"+shortName+"')";
                 System.err.println("Query:"+query);
                 int status = statement.executeUpdate(query);
                 if(status != 1){
                     return "Save failed";
                 }else{
                     return "Saved successfully";
                 }
                 
        } catch (SQLException ex) {
                                    
              Logger.getLogger(CategoryDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
    }

   @Override
   public String updateCategoryDetails(Integer id,String name,String shortName){
        try{
                        Connection connection = getConnection();
                        Statement statement = connection.createStatement();                       
                        String query = "UPDATE CATEGORYMENU SET CATNAME='"+name+"',SHORTNAME='"+shortName+"'  WHERE ID="+id+"";
                        System.out.print(query);
                        int update = statement.executeUpdate(query);
                        if(update !=1)
                            return "Updated failed";
                        else 
                            return "Upadated sucessfully";
                    }catch(SQLException ex){
                        return "Update failed";
                    }
                    
       
   }

    
}
   
