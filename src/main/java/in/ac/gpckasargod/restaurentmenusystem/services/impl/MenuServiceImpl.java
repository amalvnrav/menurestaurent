/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.services.impl;

import in.ac.gpckasargod.restaurentmenusystem.model.ui.data.MenuDetails;
import in.ac.gpckasargod.restaurentmenusystem.service.MenuService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class MenuServiceImpl extends ConnectionServiceImpl implements MenuService{

    public String saveMenuDetails(String itemName,Integer categoryId,String itemPrize) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO MAINMENU(ITEMNAME,CATEGORYID,ITEMPRIZE) VALUES " 
                    + "('"+itemName+ "','" +categoryId+ "','" +itemPrize+"')" ;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(MenuServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }
        public MenuDetails readMenuDetails(Integer Id) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM MAINMENU WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String itemName = resultSet.getString("ITEMNAME");
                Integer categoryId = resultSet.getInt("CATEGORYID");
                String itemPrize = resultSet.getString("ITEMPRIZE");
                MenuDetails mainDetails = new MenuDetails(id,itemName,categoryId,itemPrize);                
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        MenuDetails menuDetails = null;
        return menuDetails;
       
    }
    @Override
    public List<MenuDetails>getAllenuDetailses(){
        List<MenuDetails> mainMenu = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM MAINMENU";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String itemName = resultSet.getString("ITEMNAME");
                Integer categoryId = resultSet.getInt("CATEGORYID");
                String itemPrize = resultSet.getString("ITEMPRIZE");
            MenuDetails MenuDetails= new MenuDetails(id,itemName,categoryId,itemPrize);
                //MenuDetails.add((MenuDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(MenuServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return mainMenu;
    
    }
    
    public String updateMainMenu(Integer id,String itemName,Integer categoryId,String itemPrize ){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE MAINMENU SET ITEMNAME='"+itemName+"',CATEGORYID='"+categoryId+"',ITEMPRIZE='"+itemPrize+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }

    public String deleteComplaint(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM MAINMENU WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
       
       
    }

   

    @Override
    public String saveMenuDetails(String itemName, int categoryId, String itemPrize) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String updateMenuDetails(int selectedId, String itemName, Integer categoryId, String itemPrize) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteMenuDetails(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
