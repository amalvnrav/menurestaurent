/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.service;

import in.ac.gpckasargod.restaurentmenusystem.model.ui.data.CategoryDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface CategoryService {
    public String saveCategoryDetails(String name,String shortName);
    public CategoryDetails readCategoryDetails(Integer id);
    public List<CategoryDetails> getAllCategoryDetailses();
    
     public String updateCategoryDetails(Integer id,String name,String shortName);
    public String deleteCategoryDetails(Integer id);
    }