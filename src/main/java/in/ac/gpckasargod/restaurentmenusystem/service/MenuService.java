/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.service;

import in.ac.gpckasargod.restaurentmenusystem.model.ui.data.MenuDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface MenuService {
    
    public String saveMenuDetails( String itemName, int categoryId,String itemPrize);
    public MenuDetails readMenuDetails(Integer Id);
    public List<MenuDetails>getAllenuDetailses();
 
 
    public String updateMenuDetails(int selectedId, String itemName,Integer categoryId, String itemPrize);
    public String deleteMenuDetails(Integer id);
}

