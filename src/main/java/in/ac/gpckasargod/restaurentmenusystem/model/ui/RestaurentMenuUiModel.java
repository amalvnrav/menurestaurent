/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.model.ui;

/**
 *
 * @author mint
 */
public class RestaurentMenuUiModel {
    private Integer id;
    private String categoryId;
    private String name;
    private String shortName;
    private String itemName;
    private String itemCategory;
    private String itemPrize;

    public RestaurentMenuUiModel(Integer id, String categoryId, String name, String shortName, String itemName, String itemCategory, String itemPrize) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.shortName = shortName;
        this.itemName = itemName;
        this.itemCategory = itemCategory;
        this.itemPrize = itemPrize;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.itemCategory = itemCategory;
    }

    public String getItemPrize() {
        return itemPrize;
    }

    public void setItemPrize(String itemPrize) {
        this.itemPrize = itemPrize;
    }
    
    
    
}
