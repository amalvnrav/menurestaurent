/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.restaurentmenusystem.model.ui.data;

/**
 *
 * @author student
 */
public class MenuDetails {
    private Integer id;
    private String itemName;
    private Integer categoryId;
    private String itemPrize;

    public MenuDetails(Integer id, String itemName, Integer categoryId, String itemPrize) {
        this.id = id;
        this.itemName = itemName;
        this.categoryId = categoryId;
        this.itemPrize = itemPrize;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemPrize() {
        return itemPrize;
    }

    public void setItemPrize(String itemPrize) {
        this.itemPrize = itemPrize;
    }

}